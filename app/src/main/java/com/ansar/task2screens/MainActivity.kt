package com.ansar.task2screens

/**
 * Created by Zohaib Kamnbrani.
 */

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ansar.task2screens.R
import com.ansar.task2screens.models.CatResponse
import com.github.kittinunf.fuel.httpGet
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_cat.*
import com.ansar.task2screens.models.Category
import com.ansar.task2screens.models.RowModel
import com.ansar.task2screens.models.SubCategory

class MainActivity : AppCompatActivity() {

    lateinit var rowAdapter: RowAdapter
    lateinit var rows : MutableList<RowModel>
    lateinit var recyclerView : RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.recycler_view)
        rows = mutableListOf()
        rowAdapter = RowAdapter(this, rows)

        recyclerView.layoutManager = LinearLayoutManager(
            this,
            RecyclerView.VERTICAL,
            false
        )

        recyclerView.adapter = rowAdapter

        back_btn.setOnClickListener {
            super.onBackPressed()
        }

        populateData()
    }


    fun populateData(){

        "http://208.109.13.111:9090/api/Category".httpGet().responseString { request, response, result ->
            val json = result.get()
            val response = Gson().fromJson(json,CatResponse::class.java)
            Log.v("",response.toString())
            response.result.forEach {
                rows.add(RowModel(RowModel.CAT,it,false))
            }
            Log.v("","")
            runOnUiThread {
                recyclerView.adapter?.notifyDataSetChanged()
            }
        }

    }
}
