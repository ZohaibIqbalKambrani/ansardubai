package com.ansar.task2screens

/**
 * Created by Zohaib Kamnbrani.
 */

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.ansar.task2screens.R
import kotlinx.android.synthetic.main.activity_cat.*

class CatActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cat)

        back_btn.setOnClickListener {
            super.onBackPressed()
        }

        rbg.setOnCheckedChangeListener { group, checkedId ->
            Log.v("","")
            if(checkedId == rb0.id){
                f1.visibility = View.VISIBLE
            }else if(checkedId == rb1.id){

            }
        }

        rbg_f1.setOnCheckedChangeListener { group, checkedId ->
            f2.visibility = View.VISIBLE
        }
    }
}