package com.ansar.task2screens

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ansar.task2screens.R
import com.ansar.task2screens.models.RowModel

/**
 * Created by Zohaib Kamnbrani.
 */
class RowAdapter (val context: Context, var rowModels: MutableList<RowModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    /**
     * flag to restrict expand / collapse action it is already expanding / collapsing
     */
    private var actionLock = false

    class CountryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var name_tv: TextView
        internal var toggle_btn : ImageButton
        internal var container : View
        init {
            this.name_tv = itemView.findViewById(R.id.name_tv) as TextView
            this.toggle_btn = itemView.findViewById(R.id.toggle_btn) as ImageButton
            this.container = itemView
        }
    }

    class StateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var name_tv: TextView
        internal var toggle_btn : ImageButton
        internal var container : View
        init {
            this.name_tv = itemView.findViewById(R.id.name_tv) as TextView
            this.toggle_btn = itemView.findViewById(R.id.toggle_btn) as ImageButton
            this.container = itemView
        }
    }

    class CityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var name_tv: TextView
        init {
            this.name_tv = itemView.findViewById(R.id.name_tv) as TextView
        }
    }

    override fun getItemViewType(position: Int): Int {
        val type = rowModels[position].type
        return type
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewHolder: RecyclerView.ViewHolder = when (viewType) {
            RowModel.CAT -> CountryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false))
            RowModel.SUB_CAT -> StateViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_category, parent, false))
            else -> CountryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false))
        }
        return viewHolder
    }


    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        val row = rowModels[p1]

        when(row.type){
            RowModel.CAT -> {
                (p0 as CountryViewHolder).name_tv.setText(row.category.title)

                if(row.category.subCategories == null || row.category.subCategories!!.size == 0) {
                    p0.toggle_btn.visibility = View.GONE
                }
                else {
                    if(p0.toggle_btn.visibility == View.GONE){
                        p0.toggle_btn.visibility = View.VISIBLE
                    }

                    if (row.isExpanded) {
                        p0.toggle_btn.background =
                            ContextCompat.getDrawable(context, R.drawable.down_ic)
                    } else {
                        p0.toggle_btn.background =
                            ContextCompat.getDrawable(context, R.drawable.forward_ic)
                    }

                    p0.container.setOnClickListener {
                        if (!actionLock) {
                            actionLock = true
                            if (row.isExpanded) {
                                row.isExpanded = false
                                collapse(p1)
                            } else {
                                row.isExpanded = true
                                expand(p1)
                            }
                        }
                    }
                }
            }
            RowModel.SUB_CAT -> {
                (p0 as StateViewHolder).name_tv.setText(row.subCategory.title)

                if(p0.toggle_btn.visibility == View.INVISIBLE){
                    p0.toggle_btn.visibility = View.VISIBLE
                }

                if (row.isExpanded) {
                    p0.toggle_btn.background =
                        ContextCompat.getDrawable(context, R.drawable.down_ic)
                } else {
                    p0.toggle_btn.background =
                        ContextCompat.getDrawable(context, R.drawable.forward_ic)
                }

                p0.container.setOnClickListener {
                    context.startActivity(Intent(context, CatActivity::class.java))
                }
            }
        }
    }


    fun expand(position: Int) {

        var nextPosition = position

        val row = rowModels[position]

        when (row.type) {

            RowModel.CAT -> {

                /**
                 * add element just below of clicked row
                 */
                for (state in row.category.subCategories!!) {
                    rowModels.add(++nextPosition, RowModel(RowModel.SUB_CAT, state))
                }

                notifyDataSetChanged()
            }

            RowModel.SUB_CAT -> {

                notifyDataSetChanged()
            }
        }

        actionLock = false
    }

    fun collapse(position: Int) {
        val row = rowModels[position]
        val nextPosition = position + 1

        when (row.type) {

            RowModel.CAT -> {

                /**
                 * remove element from below until it ends or find another node of same type
                 */
                outerloop@ while (true) {
                    if (nextPosition == rowModels.size || rowModels.get(nextPosition).type === RowModel.CAT) {
                        break@outerloop
                    }

                    rowModels.removeAt(nextPosition)
                }

                notifyDataSetChanged()
            }

            RowModel.SUB_CAT -> {

                /**
                 * remove element from below until it ends or find another node of same type or find another parent node
                 */
                outerloop@ while (true) {
                    if (nextPosition == rowModels.size || rowModels.get(nextPosition).type === RowModel.CAT || rowModels.get(nextPosition).type === RowModel.SUB_CAT
                    ) {
                        break@outerloop
                    }

                    rowModels.removeAt(nextPosition)
                }

                notifyDataSetChanged()
            }
        }

        actionLock = false
    }


    override fun getItemCount() = rowModels.size

}