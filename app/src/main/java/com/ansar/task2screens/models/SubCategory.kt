package com.ansar.task2screens.models

/**
 * Created by Zohaib Kamnbrani .
 */
data class SubCategory(val id : String, var title : String) {}