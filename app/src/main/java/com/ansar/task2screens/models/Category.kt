package com.ansar.task2screens.models


/**
 * Created by Zohaib Kamnbrani.
 */
data class Category(var id :String, val title : String, var subCategories : MutableList<SubCategory>?) {}