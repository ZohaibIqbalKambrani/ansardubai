package com.ansar.task2screens.models

import androidx.annotation.IntDef

/**
 * Created by Zohaib Kamnbrani.
 */
class RowModel {

    companion object {

        @IntDef(CAT, SUB_CAT)
        @Retention(AnnotationRetention.SOURCE)
        annotation class RowType

        const val CAT = 1
        const val SUB_CAT = 2
    }

    @RowType var type : Int

    lateinit var category : Category

    lateinit var subCategory : SubCategory


    var isExpanded : Boolean

    constructor(@RowType type : Int, category : Category, isExpanded : Boolean = false){
        this.type = type
        this.category = category
        this.isExpanded = isExpanded
    }

    constructor(@RowType type : Int, subCategory : SubCategory, isExpanded : Boolean = false){
        this.type = type
        this.subCategory = subCategory
        this.isExpanded = isExpanded
    }

}