package com.ansar.task2screens.models

import com.ansar.task2screens.models.Category
import com.ansar.task2screens.models.SubCategory

/**
 * Created by Zohaib Kamnbrani.
 */
internal data class CatResponse(var result:  MutableList<Category>) {}